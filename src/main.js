import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import currencyFilter from '@/filters/currency.filter.js'
import localizeFilter from '@/filters/localize.filter.js'
import tooltipDirective from '@/directives/tooltip.directive.js'
import messagePlugin from '@/utils/message.plugin'
import titlePlugin from '@/utils/title.plugin'
import Loader from '@/components/app/Loader'
import Paginate from 'vuejs-paginate'
import VueMeta from 'vue-meta'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'


import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

Vue.config.productionTip = false
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.filter('localize', localizeFilter)
Vue.directive('tooltip', tooltipDirective)
Vue.use(Vuelidate)
Vue.use(messagePlugin)
Vue.use(VueMeta)
Vue.use(titlePlugin)
Vue.component('Loader', Loader)
Vue.component('Paginate', Paginate)


firebase.initializeApp({
  apiKey: "AIzaSyDsbPVmbywBJQisH-z90d2i2q01PD4DCfU",
  authDomain: "vue-crm-ebc49.firebaseapp.com",
  databaseURL: "https://vue-crm-ebc49.firebaseio.com",
  projectId: "vue-crm-ebc49",
  storageBucket: "vue-crm-ebc49.appspot.com",
  messagingSenderId: "929329083227",
  appId: "1:929329083227:web:b5eb1c206867906a7fe07e",
  measurementId: "G-PB474ER307"
});


let app;
firebase.auth().onAuthStateChanged(() => {
  if(!app){
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})



